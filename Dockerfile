FROM node:10-alpine

WORKDIR /app

COPY /code/package*.json /app/
COPY ./code/public/* /app/public/
COPY  ./code/index.js /app

RUN npm install

CMD ["node", "index.js"]
